# Set the base image using an image from https://registry.videolan.org/v2/vlc-debian-android/tags/list.
FROM registry.videolan.org/vlc-debian-android:20220505164734

# Set the working directory.
WORKDIR /vlc-android

# Set the environment variables for the Android NDK and SDK.
ENV ANDROID_NDK="/sdk/android-ndk"
ENV ANDROID_SDK="/sdk/android-sdk-linux"
